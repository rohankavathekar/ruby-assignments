require_relative 'input_taker.rb'

class Product
  attr_accessor :product, :quantity :price
  def initialize(product,quantity,price)
    @product = product
    @quantity = quantity
    @price =price
  end

  def set_products
    #setting individual items here
  end

  def unit_prices
    @items.each_with_object({}) do |items,hash|
      hash[items] = #setting indivisual prices
    end
  end

  def sale_prices
     @items.each_with_object({}) do |items,hash|
      hash[items] = #setting sale prices

  end

#in this class the items are stored in hash with indivisual prices
