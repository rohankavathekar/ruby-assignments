require_relative 'file_handler.rb'
class Product

  def initialize
    @file = FileHandler.new("Inventory.txt")
    # @id = Time.now.usec             can use time-stamp as id
    @hash = {
      :id => Integer,
      :name => String,
      :company => String,
      :price => Integer,
      :quantity => Integer
    }
  end

  def add
    product = {}
    @hash.each do |key,value|
      puts "Enter #{key}"
      product[key] = gets.chomp
      puts "#{product[key]} added"
    end
    puts product
    @file.add(product)
  end

  def list
    @file.index
  end

  def search(name)
    @file.search(name)
  end

  def buy(name)
    @file.buy(name)
  end

  def remove(name)
    @file.remove(name)
  end

  def update
    puts "updating Products here"
  end

end
