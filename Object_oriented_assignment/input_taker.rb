require_relative 'product.rb'
class InputTaker
  def initialize(choice)
    @choice = choice
    @product = Product.new
  end

  def product_calls
    case @choice
    when 1
      @product.list

    when 2
      puts "Enter Product id To Search"
      name = gets.chomp
      @product.search(name)

    when 3
      puts "Enter Product Name To Buy"
      name = gets.chomp
      @product.buy(name)

    when 4
      @product.add
    when 5
      puts "Enter Product Name To Remove"
      name = gets.chomp
      @product.remove(name)

    when 6
      puts "Enter Product Name To Update"
      name = gets.chomp
      @product.update

    else
      puts "Invalid Choice"
    end
  end

end

