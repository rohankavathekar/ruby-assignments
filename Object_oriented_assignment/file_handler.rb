class FileHandler
  SEP = ("\n"+"\n")

  def initialize(file_name = "default.txt")
    @file_name = File.open(file_name, "a+")
  end

  def add(product)
    @product = product
    @product.each do |key, value|
      @file_name.puts "#{key}: #{value}"
    end
    @file_name.write(SEP)
    puts "Entry Saved"
  end

  def index
    puts @file_name.readlines
  end

  def search(name)
    @file_name.each(SEP) do |line|
      puts line if line.include? name
    end
  end

  def remove(name)
    @file.each do |line|
      if line.include? "apple"
        file.seek(-line.length, IO::SEEK_CUR)
        file.write(' ' * (line.length-1))
        file.write("\n")
        puts "Removed Record Successfully!"
      end
    end
  end

end

obj = FileHandler.new "Inventory.txt"
# obj.add
# obj.index
# obj.search
obj.remove
