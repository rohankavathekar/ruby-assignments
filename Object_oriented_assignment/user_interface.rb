require_relative 'input_taker.rb'
class UserInterface

  def initialize
    puts "Enter User Type"
    puts "1. Customer\n2. ShopKeeper"
    @user_type =gets.to_i
  end

  def modes
    case @user_type
    when 1
      puts "Welcome To The Customer Mode"
      puts "\n Select Options\n1.List Product\n2.Search Product\n3.Buy Product"
      choice = gets.to_i
    when 2
      puts "Welcome To The Shopkeeper Mode"
      puts "\n Select Options\n1.List Product\n2.Search Product\n3.Buy Product"
      puts "4.Add Product\n5.Remove Product\n6.Update Product"
      choice = gets.to_i
    else
      puts "Please Enter Valid Choice"
    end
    choice = InputTaker.new(choice)
    choice.product_calls
  end
end

obj = UserInterface.new
obj.modes