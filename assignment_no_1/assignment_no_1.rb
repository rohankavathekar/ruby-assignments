#string methods
string_1 = "I am learning Ruby language."
string_2 = "why? Coz it's cool and I like it."

puts string_1.split.each{|i| i.capitalize}.join(' ')
puts string_1.swapcase
puts string_1.length
puts string_1.include?('ruby')
puts string_2.split('?')
puts string_1.concat(string_2)
string_1.gsub! 'I', 'We'
puts string_1.gsub 'am', 'are'
my_symbol = string_1.to_sym
puts my_symbol.class

#check alphabet with if else statement
def check_alphabet(letter)
  if letter=='a' || letter=='e' || letter=='i' || letter=='o' || letter=='u' ||
     letter=='A' || letter=='E'  || letter== 'I' || letter=='O'  || letter=='U'
    puts "#{letter} is vowel"
  else
    puts "#{letter} is Consonent"
  end
end
 check_alphabet("f")


#fibonocci with for loop
def fibonnoci(limit)
first = 0
second = 1
  for i in 1..limit do
    third = first + second
    puts second
    first = second
    second = third
  end
end
fibonnoci(10)

#calculator with switch case
def calculator(operator,number_1,number_2)
  case operator
  when '+'
    puts number_1 + number_2
  when '-'
    puts number_1 - number_2
  when '*'
    puts number_1 * number_2
  when '/'
    puts number_1 / number_2
  end
end
calculator('+',12,5)
