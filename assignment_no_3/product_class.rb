require_relative 'save_file.rb'
class Product
  # attr_accessor :product_id ,:product_name, :product_price, :product_quantity, :product_company
  def initialize(item_access)
    product_hash = {}
    @stamp = Time.now.usec
    @item_access = item_access
  end

  def product_calls
    case @item_access
    when 1
      list_items
    when 2
      search_items
    when 3
      buy_items
    when 4
      add_items
    when 5
      remove_items
    when 6
      update_items
    end
  end

  def list_items
    puts "listing all items here"
    list=FileOperations.new
    list.list_all
  end
  def search_items
    puts "Enter Product Name To search"
    get_name = gets.chomp
    search=FileOperations.new
    search.search_in_file(get_name)
  end
  def buy_items
    customer_details = Hash.new
    puts "Enter Product Name To buy"
    buy_product = gets.chomp
    customer_details["product"] = buy_product

    puts "add Credit Card Info:"
    credit_card = gets.chomp.to_i
    customer_details["credit_card"] = credit_card
    puts "Credit Card added : #{credit_card} "

    puts "add CVV:"
    credit_cvv = gets.chomp.to_i
    customer_details["CVV"] = credit_cvv
    puts "CVV added : #{credit_cvv} "

    buy=FileOperations.new
    buy.buying_item(customer_details)
    puts "your order of #{buy_product} will be Reached in 3-5 Business days! "
  end
  def add_items
    hash_of_orders = Hash.new
    puts "add product name"
    product_name = gets.chomp
    hash_of_orders["product_name"] = product_name
    hash_of_orders["product_id"] = @stamp
    puts "#{product_name} Added with product id #{@stamp}!"

    puts "add product company"
    product_company = gets.chomp
    hash_of_orders["product_company"] = product_company
    puts " #{product_name} company:#{product_company}!"

    puts "add product price"
    product_price = gets.chomp.to_i
    hash_of_orders["product_price"] = product_price
    puts "Price of #{product_name} is #{product_price}rs!"

    puts "add product quantity"
    product_quantity = gets.chomp.to_i
    hash_of_orders["product_quantity"] = product_quantity
    puts "#{product_quantity} #{product_name}'s are added to Inventory !"

    sending_object=FileOperations.new
    sending_object.order_in_file(hash_of_orders)
  end
  def remove_items
    puts "Enter Product Name To Remove"
    get_name = gets.chomp
    name_for_remove=FileOperations.new
    name_for_remove.remove_from_file(get_name)
  end
  def update_items
    puts "Enter Product Name To Update"
    get_name = gets.chomp
    name_to_update=FileOperations.new
    name_to_update.update_from_file(get_name)
  end
end