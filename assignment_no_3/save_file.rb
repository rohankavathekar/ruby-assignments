class FileOperations
  REGX = /"?{?\\/

  def list_all
    read = File.open("new_data.txt", "r")
    lines = read.readlines.to_s.gsub(/, /,"\n")
    puts lines.gsub!(REGX,"")
  end

  def search_in_file(get_name)
    @get_name = get_name
    text=File.open('inventory.txt').read
    text.each_line do |line|
       if line.include? @get_name
        line.gsub!(/, /,"\n")
        puts line
      end
    end
  end

  def buying_item(customer_details)
    @customer_details = customer_details.to_s
    file_content = File.write("orders.txt", @customer_details + "\n", mode: "a")
  end

  def order_in_file(hash_of_orders)
    @hash_of_orders = hash_of_orders.to_s
    file_content = File.write("inventory.txt", @hash_of_orders + "\n", mode: "a")
  end

  def remove_from_file(get_name)
    @get_name = get_name
    file = File.new("inventory.txt", 'r+')
    file.each do |line|
      if line.include? @get_name
        file.seek(-line.length, IO::SEEK_CUR)
        file.write(' ' * (line.length-1))
        file.write("\n")
        puts "Removed Record Successfully!"
      end
    end
    file.close
  end

  def update_from_file(get_name)
    @get_name = get_name
    file = File.new("inventory.txt", 'r+')
    file.each do |line|
      if line.include? @get_name
        puts line
      end
    end
    file.close
  end
end