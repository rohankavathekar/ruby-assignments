array_1 = [2, 4, 6, 8, 10]
array_2 = [1, 5, 6, 8, 11, 12]
hash_1 = {a: 'a', b: 'b', c: 'c', d: 'd', e: 'e'}
hash_2 = {x: '10', y: '20', z: '30'}
new_hash = {}

10.times{print "hello"," "}
(30..40).each{|element| print element," "}

print (array_1 + array_2).uniq
print (array_1 + array_2).select(&:even?)
print (array_1 + array_2).select{|element| element > 8}
array_1.inject(0){|sum,element| print sum + element*element*element}
array_1.each{|element| print 5 + element," "}

hash_2.values.inject(0){|hash_sum,element| hash_sum +=element.to_i }

keys_array = hash_1.keys
new_hash = Hash[keys_array.zip array_1]
