#block problem
def calculator(a1,a2,&add)
    add.call(a1,a2)
end

calculator(5,6) { |num,num2| puts num+num2 }
calculator(5,6) { |num,num2| puts num-num2 }
calculator(5,6) { |num,num2| puts num*num2 }
calculator(5,6) { |num,num2| puts num/num2 }

# Proc problem
def compose
a = @proc_one.call(5)
@proc_two.call(a)
end
@proc_one = Proc.new{ |ele| ele * 2 }
@proc_two = Proc.new{ |ele| puts ele * ele }


#lambda Problem
def lambda_compose
a = @lamb_one.call(15)
@lamb_two.call(a)
end
@lamb_one = ->(x) { x * 2 }
@lamb_two = ->(x) { x * x }
