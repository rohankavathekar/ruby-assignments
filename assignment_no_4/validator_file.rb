require 'csv'
 REG= /<[^>]*>(&?[a-z]+?;?)*/
 #regex for text within text ---->
# REGEX = /(?<=<t[a-z]>)(.?[a-z]+;)*?\s?[\w+0-9]+-?\s?[\w+0-9]+-?\s?[\w+0-9]+(?=<\/t[a-z]>)/

class ValidatorFile
  def html_reader
    temp = File.foreach('submission_report.html').map { |line| line.split("</tr>").join("|") }
    @data = temp.to_s
  end

  def valid?
  p @filtered = @data.gsub(REG, '').split("|")
  end

  def csv_save
    file = CSV.open("submission_report.csv", "a") do |csv|
    csv << [@filtered]
    end
  end
end

obj = ValidatorFile.new
obj.html_reader
obj.valid?
obj.csv_save
